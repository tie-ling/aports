# Maintainer: Luca Weiss <luca@z3ntu.xyz>
pkgname=py3-dbusmock
_pyname=python-dbusmock
pkgver=0.31.0
pkgrel=0
pkgdesc="Mock D-Bus objects for tests"
url="https://github.com/martinpitt/python-dbusmock"
arch="noarch"
license="LGPL-3.0-or-later"
depends="dbus py3-dbus py3-gobject3"
makedepends="python3-dev py3-setuptools"
checkdepends="bash py3-nose py3-pytest"
_pypiprefix="${_pyname%${_pyname#?}}"
subpackages="$pkgname-pyc"
source="https://files.pythonhosted.org/packages/source/$_pypiprefix/$_pyname/$_pyname-$pkgver.tar.gz"
builddir="$srcdir/$_pyname-$pkgver"

build() {
	python3 setup.py build
}

check() {
	# Skip the fakeroot tests as they can't run under our fakeroot environment
	# https://github.com/martinpitt/python-dbusmock/issues/46
	env -u LD_PRELOAD python3 -m unittest
}

package() {
	python3 setup.py install --skip-build --root="$pkgdir"
}

sha512sums="
dae84c8c99b85942ee5f50b25f6dc8df1190c1fcc2eba30de449f767ddc6342308b2083ef02e4a0e0a819f1632e146cf9a382aa0cdd962708ab29fb14f68d861  python-dbusmock-0.31.0.tar.gz
"
